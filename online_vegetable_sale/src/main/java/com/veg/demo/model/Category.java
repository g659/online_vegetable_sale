package com.veg.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table
public class Category {
	
	@Id
	@GeneratedValue
	public Integer Category_ID;
	
	public String Category_Name;
	
	public String Discription; 
	public String Picture;
	public Integer getCategory_ID() {
		return Category_ID;
	}
	public void setCategory_ID(Integer category_ID) {
		Category_ID = category_ID;
	}
	public String getCategory_Name() {
		return Category_Name;
	}
	public void setCategory_Name(String category_Name) {
		Category_Name = category_Name;
	}
	public String getDiscription() {
		return Discription;
	}
	public void setDiscription(String Discription) {
		Discription = Discription;
	}
	public String getPicture() {
		return Picture;
	}
	public void setPicture(String picture) {
		Picture = picture;
	}
	@Override
	public String toString() {
		return "Category [Category_ID=" + Category_ID + ", Category_Name=" + Category_Name + ", Discription="
				+ Discription + ", Picture=" + Picture + "]";
	}
	public Category(Integer category_ID, String category_Name, String discription, String picture) {
		super();
		Category_ID = category_ID;
		Category_Name = category_Name;
		Discription = discription;
		Picture = picture;
	}
	
	
}
