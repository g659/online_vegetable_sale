package com.veg.demo.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class MyCart {
	
	@Id
	@GeneratedValue
	public Integer cartNumber;
	
	@Column
	public String itemName;
	
	@Column
	public int quntityPerItem;
	
	@Column
	public int totalquntity;
	
	@Column
	public Date purchesDate;
	
	@Column
	public Date deliveryDate;
	
	@Column
	public String deliveryAddress;
	
	@Column
	public Double price;
	
	@Column
	public Double totalAmount;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "item_id", nullable = false)
    private MyCart cart;

	public MyCart() {
		super();
	}

	public MyCart getCart() {
		return cart;
	}

	public void setCart(MyCart cart) {
		this.cart = cart;
	}

	public MyCart(Integer cartNumber, String itemName, int quntityPerItem, int totalquntity, Date purchesDate,
			Date deliveryDate, String deliveryAddress, Double price, Double totalAmount) {
		super();
		this.cartNumber = cartNumber;
		this.itemName = itemName;
		this.quntityPerItem = quntityPerItem;
		this.totalquntity = totalquntity;
		this.purchesDate = purchesDate;
		this.deliveryDate = deliveryDate;
		this.deliveryAddress = deliveryAddress;
		this.price = price;
		this.totalAmount = totalAmount;
	}

	public Integer getCartNumber() {
		return cartNumber;
	}

	public void setCartNumber(Integer cartNumber) {
		this.cartNumber = cartNumber;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public int getQuntityPerItem() {
		return quntityPerItem;
	}

	public void setQuntityPerItem(int quntityPerItem) {
		this.quntityPerItem = quntityPerItem;
	}

	public int getTotalquntity() {
		return totalquntity;
	}

	public void setTotalquntity(int totalquntity) {
		this.totalquntity = totalquntity;
	}

	public Date getPurchesDate() {
		return purchesDate;
	}

	public void setPurchesDate(Date purchesDate) {
		this.purchesDate = purchesDate;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getDeliveryAddress() {
		return deliveryAddress;
	}

	public void setDeliveryAddress(String deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}

	@Override
	public String toString() {
		return "MyCart [cartNumber=" + cartNumber + ", itemName=" + itemName + ", quntityPerItem=" + quntityPerItem
				+ ", totalquntity=" + totalquntity + ", purchesDate=" + purchesDate + ", deliveryDate=" + deliveryDate
				+ ", deliveryAddress=" + deliveryAddress + ", price=" + price + ", totalAmount=" + totalAmount + "]";
	}
	
	

}
