package com.veg.demo.model;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table
public class AddItem {
	
	@Id
	@GeneratedValue
	public Integer itemCode;
	

    @OneToMany(mappedBy = "cart", fetch = FetchType.LAZY,
            cascade = CascadeType.ALL)
    private Set<MyCart> cart; 
	
	@Column
	public String itemName;
	@Column
	public float itemPrice;
	@Column
	public Integer itemQuanity;
	@Column
	public String imagePath;
	
	@Column
	public Double stock;
	@Column
	public String typeCategory;
	@Column
	public String Description;
	@Column
	public Integer status;
	@Column
	public Integer Action;

	public Integer getItemCode() {
		return itemCode;
	}

	public void setItemCode(Integer itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public float getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(float itemPrice) {
		this.itemPrice = itemPrice;
	}

	public Integer getItemQuanity() {
		return itemQuanity;
	}

	public void setItemQuanity(Integer itemQuanity) {
		this.itemQuanity = itemQuanity;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public Double getStock() {
		return stock;
	}

	public void setStock(Double stock) {
		this.stock = stock;
	}
	

	public String getTypeCategory() {
		return typeCategory;
	}

	public void setTypeCategory(String typeCategory) {
		this.typeCategory = typeCategory;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getAction() {
		return Action;
	}

	public void setAction(Integer action) {
		Action = action;
	}

	public Set<MyCart> getCart() {
		return cart;
	}

	public void setCart(Set<MyCart> cart) {
		this.cart = cart;
	}

	@Override
	public String toString() {
		return "AddItem [itemCode=" + itemCode + ", itemName=" + itemName + ", itemPrice=" + itemPrice
				+ ", itemQuanity=" + itemQuanity + ", imagePath=" + imagePath + ", stock=" + stock + ", typeCategory="
				+ typeCategory + ", Description=" + Description + ", status=" + status + ", Action=" + Action + "]";
	}


	

}
