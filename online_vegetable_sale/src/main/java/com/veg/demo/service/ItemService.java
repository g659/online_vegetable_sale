package com.veg.demo.service;

import java.util.List;

import com.veg.demo.model.AddItem;

/*
 * Radhika
 */
public interface ItemService {
	
	public void addNewItem(AddItem addItem);
	
	public AddItem updatItem(AddItem item);
	
	public void deleteItem(Integer itemCode);
	
	public List<AddItem> getAllItem();

}
