package com.veg.demo.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.veg.demo.model.AddItem;
import com.veg.demo.repo.ItemRepoImpl;

/*
 * Radhika
 */
@Service
public class ItemServiceImpl implements ItemService{
	
	@Autowired
	private ItemRepoImpl itemRpo;

	@Override
	@Transactional
	public void addNewItem(AddItem addItem) {
		itemRpo.addNewItem(addItem);
		
	}

	@Override
	@Transactional
	public AddItem updatItem(AddItem item) {
		return itemRpo.updateItem(item);
	}

	@Override
	@Transactional
	public void deleteItem(Integer itemCode) {
		itemRpo.deleteItem(itemCode);
	}

	@Override
	@Transactional
	public List<AddItem> getAllItem() {
		// TODO Auto-generated method stub
		return null;
	}

}
