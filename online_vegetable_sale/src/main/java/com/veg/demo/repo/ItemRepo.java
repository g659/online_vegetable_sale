package com.veg.demo.repo;

import java.util.List;

import com.veg.demo.model.AddItem;

/*
 * Radhika
 */
public interface ItemRepo {
	
	public AddItem updateItem(AddItem item);
	
	public void addNewItem(AddItem addItem);
	
	public void deleteItem(Integer itemCode);
	
	public List<AddItem> getAllListItem();

}
