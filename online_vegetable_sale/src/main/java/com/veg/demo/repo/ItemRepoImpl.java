package com.veg.demo.repo;

import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.veg.demo.model.AddItem;

/*
 *  created by Radhika
 */
@Repository
public class ItemRepoImpl implements ItemRepo{
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	@Transactional
	public AddItem updateItem(AddItem item) {
		sessionFactory.getCurrentSession().update(item);
		return item;
	}

	/*
	 * Radhika
	 */
	@Override
	@Transactional
	public void addNewItem(AddItem addItem) {
		
		sessionFactory.getCurrentSession().saveOrUpdate(addItem);
		
	}

	/*
	 * Radhika
	 */
	@Override
	@Transactional
	public void deleteItem(Integer itemCode) {
		AddItem item = (AddItem) sessionFactory.getCurrentSession().load(
				AddItem.class, itemCode);
		if (null != item) {
			this.sessionFactory.getCurrentSession().delete(item);
		}
	}

	@Override
	@Transactional
	public List<AddItem> getAllListItem() {
		return sessionFactory.getCurrentSession().createQuery("from Employee")
				.list();
	}

}
