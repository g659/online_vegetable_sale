package com.veg.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineVegitableSaleApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineVegitableSaleApplication.class, args);
		System.out.println("Hi team!");
	}

}
