package com.veg.demo.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.veg.demo.model.AddItem;
import com.veg.demo.service.ItemService;

/*
 * Created by Radhika
 */
@RestController
public class AdminController {
	
	
	@Autowired
	private ItemService itemService;
	
	@RequestMapping("/")
	public String home() 
	{
		
		return "veghome.jsp";
	}
	
	/*
	 * Radhika
	 * Creating or Updating item
	 * 
	 */
	@RequestMapping(value = "/addNewItem", method = RequestMethod.POST)
	public ModelAndView addNewItem(@ModelAttribute AddItem addItem) {
		if(addItem.getItemCode() == 0) {
			itemService.addNewItem(addItem);
		}else {
			itemService.updatItem(addItem);
		}
		return new ModelAndView("redirect:/");
	}
	
	/*
	 * Radhika
	 * Deleting Item
	 * 
	 */
	@RequestMapping(value = "/deleteItem", method = RequestMethod.GET)
	public ModelAndView deleteItem(javax.servlet.http.HttpServletRequest request) {
		int itemCode = Integer.parseInt(request.getParameter("itemCode"));
		itemService.deleteItem(itemCode);
		return new ModelAndView("redirect:/");
	}
	/*
	 * View Item
	 * 
	 */
	
	
	@RequestMapping(value = "/lisItems", method = RequestMethod.GET)
	public ModelAndView listEmployee(ModelAndView model) throws java.io.IOException {
		List<AddItem> listItem = itemService.getAllItem();
		model.addObject("listAllItem", listItem);
		model.setViewName("veghome");
		return model;
	}

}
